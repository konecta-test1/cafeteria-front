import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { NzNotificationRef, NzNotificationService } from 'ng-zorro-antd/notification';
import { finalize } from 'rxjs';
import { AuthService } from './auth/services/auth.service';
import { InactivityModalComponent } from './shared/components/inactivity-modal/inactivity-modal.component';
import { LocalStorageService } from './shared/services/local-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit
 {
  isCollapsed = false;

  constructor(
      private idle: Idle, 
      cd: ChangeDetectorRef,
      private _authService : AuthService,
      private _localStorageService : LocalStorageService,
      private _modalService: NzModalService
    ) {
    // set idle parameters
    idle.setIdle(300); // how long can they be inactive before considered idle, in seconds
    idle.setTimeout(15); // how long can they be idle before considered timed out, in seconds
    idle.setInterrupts(DEFAULT_INTERRUPTSOURCES); // provide sources that will "interrupt" aka provide events indicating the user is active

    // do something when the user becomes idle
    idle.onIdleStart.subscribe(() => {
      this._modalService.warning({
        nzTitle: 'Se ha detectado inactividad',
        nzContent : InactivityModalComponent,
      })
    });
    // do something when the user is no longer idle
    idle.onIdleEnd.subscribe(() => {
      this._modalService.closeAll();
    });

    // do something when the user has timed out
    idle.onTimeout.subscribe(() => {
      this._modalService.closeAll();
      this._authService.logout().pipe(
        finalize( () => this._localStorageService.clearTokenStorageAndRedirectLogin("/auth/login") )
      ).subscribe();
    });
    
  }

  resetIdle() {
    // we'll call this method when we want to start/reset the idle process
    // reset any component state and be sure to call idle.watch()
    this.idle.watch();
  }

  ngOnInit(): void {
    // right when the component initializes, start reset state and start watching
    if (this._authService.isAuthenticated()) this.resetIdle ();
  }
}
