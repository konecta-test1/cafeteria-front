import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzModalService } from 'ng-zorro-antd/modal';
import { filter, finalize, map, Observable } from 'rxjs';
import { ProductInterface } from 'src/app/admin/interfaces/product/product-interface';
import { CreateSaleInterface } from 'src/app/admin/interfaces/sale/create-sale-interface';
import { ProductSaleInterface } from 'src/app/admin/interfaces/sale/product-sale-interface';
import { ProductService } from 'src/app/admin/services/product.service';
import { AuthService } from 'src/app/auth/services/auth.service';
import { ResultStatus } from 'src/app/shared/enums/result-status.enum';
import { ResultInterface } from 'src/app/shared/interfaces/result-interface';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { PermissionsUserService } from 'src/app/shared/services/permissions-user.service';
import { SaleService } from '../services/sale.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {

  productSelectedId : number = 0;
  userName : string;
  inputQuantity : FormControl = new FormControl(1,[Validators.min(1)]);
  products$! : Observable<ProductInterface[]>;
  isLoggingOut : boolean = false;
  isBuying : boolean = false;
  permissionsRequired = {
    panel : false
  };
  result : ResultInterface = {
    show : false,
    status : ResultStatus.SUCCESS,
    title : '',
    description : ''
  }

  constructor(
    private _productService : ProductService,
    private _modal : NzModalService,
    private _saleService : SaleService,
    private _localStorageService : LocalStorageService,
    public readonly _permissionUserService : PermissionsUserService,
    private _authService : AuthService,
    private _router : Router
  ) {
    this.userName = this._localStorageService.getUser()?.name ?? '';
   }

  ngOnInit(): void {
    this.checkPermissionsOfUser();
    this.getAllProducts();
  }

  checkPermissionsOfUser(){
    this.permissionsRequired.panel = this._permissionUserService.checkPermissions(['access.panel']);
  }

  getAllProducts(){
    this.products$ = this._productService.getAllProducts().pipe(
      map( (resp) => resp.data ),
      map( (products) => {
        return products.filter((product)=> product.stock > 0)
      })
    );
  }

  plusQuantity(){
    this.inputQuantity.setValue(this.inputQuantity.value + 1);
  }

  minusQuantity(){
    this.inputQuantity.setValue(this.inputQuantity.value - 1);
  }

  toSelectProduct(productId:number){
    if (this._authService.isAuthenticated()) {
      this.productSelectedId = productId;
      this.inputQuantity.setValue(1);
    }else{
      this._router.navigateByUrl('/auth/login');
    }
  }

  confirmPurchase(){
    this._modal.confirm({
      nzTitle: 'Deseas realizar la compra?',
      nzContent: 'Haz click en OK para continuar',
      nzOnOk: () => this.toBuy()
    });
  }

  toBuy(){
    const user = this._localStorageService.getUser();
    if (user) {
      let sale : CreateSaleInterface = {
        user_id : user.id,
        products : [
          {
            product_id : this.productSelectedId,
            quantity : this.inputQuantity.value
          }
        ]
      }
      
      this.isBuying = true;
      this._saleService.storeSale(sale).pipe(
        finalize( () => this.isBuying = false )
      ).subscribe({
        next: (resp) => {
          this.showResult(ResultStatus.SUCCESS,'Compra procesada correctamente!',`Orden Nº ${resp.data.id}`);
        },
        error: (resp) => {
          this.showResult(ResultStatus.ERROR,'Error al procesar la compra!',``);
          console.log(resp);
          
        }
      })


    }else{
      this._router.navigateByUrl('/auth/login');
    }
  }

  toLogout(){
    this.isLoggingOut = true;
    this._authService.logout().pipe(
      finalize( () => {
        this.isLoggingOut = false;
        this._localStorageService.clearTokenStorageAndRedirectLogin();
        this.userName = '';
      })
    ).subscribe();
  }

  showResult(status:ResultStatus,title:string,description:string){
    this.result = {
      show : true,
      status,
      title,
      description
    }
  }

  hideResult(){
    this.productSelectedId = 0;
    this.result = {
      show : false,
      status : ResultStatus.SUCCESS,
      title : '',
      description : ''
    }
  }

}
