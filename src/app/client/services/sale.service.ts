import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CreateSaleInterface } from 'src/app/admin/interfaces/sale/create-sale-interface';
import { SaleInterface } from 'src/app/admin/interfaces/sale/sale-interface';
import { StandarApiResponseInterface } from 'src/app/admin/interfaces/standar-api-response-interface';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SaleService {

  constructor(
    private _http : HttpClient
  ) { }

  storeSale(dataSale:CreateSaleInterface) : Observable<StandarApiResponseInterface<SaleInterface>> {
    return this._http.post<StandarApiResponseInterface<SaleInterface>>(`${environment.apiUrl}/sales`,dataSale);
  }
}
