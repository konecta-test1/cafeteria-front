import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StandarApiResponseInterface } from 'src/app/admin/interfaces/standar-api-response-interface';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { environment } from 'src/environments/environment';
import { LoginInterface } from '../interfaces/login-interface';
import { TokenInterface } from '../interfaces/token-interface';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private _http : HttpClient,
    private _localStorageService : LocalStorageService
  ) { }

  login(dataLogin : LoginInterface) : Observable<StandarApiResponseInterface<TokenInterface>> {
    return this._http.post<StandarApiResponseInterface<TokenInterface>>(`${environment.apiUrl}/auth/login`,dataLogin);
  }

  isAuthenticated() : Boolean{
    return this._localStorageService.getToken() ? true : false;
  }

  refreshToken() : Observable<StandarApiResponseInterface<TokenInterface>>{
    let refresh_token = this._localStorageService.getRefreshToken();
    return this._http.post<StandarApiResponseInterface<TokenInterface>>(`${environment.apiUrl}/auth/refresh`,{ refresh_token });
  }

  logout() : Observable<string>{
    return this._http.post<string>(`${environment.apiUrl}/auth/logout`,{});
  }
}
