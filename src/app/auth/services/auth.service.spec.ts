import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';

import { AuthService } from './auth.service';

describe('AuthService', () => {
  let service: AuthService;
  let httpClientSpy : { post : jasmine.Spy };

  beforeEach(() => {
    TestBed.configureTestingModule({});
    httpClientSpy = jasmine.createSpyObj('HttpClient',['post']);
    service = new AuthService(httpClientSpy as any , new LocalStorageService(jasmine.createSpyObj("Router", ['navigate'])) );
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
