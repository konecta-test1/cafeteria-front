import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, tick } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { NzMessageModule, NzMessageService } from 'ng-zorro-antd/message';
import {NgIdleModule} from '@ng-idle/core';
import { LoginComponent } from './login.component';
import { AuthService } from '../services/auth.service';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authServiceSpy : { login : jasmine.Spy };
  let routerSpy : { navigate : jasmine.Spy };

  beforeEach(async () => {
    authServiceSpy = jasmine.createSpyObj('AuthService', ['login']);
    routerSpy = jasmine.createSpyObj('Router', ['navigate']);

    await TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NzMessageModule,
        NgIdleModule.forRoot(),
      ],
      providers: [
        NzMessageService,
        { provide: AuthService, useValue: authServiceSpy },
        { provide: Router, useValue: routerSpy }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('login form must be invalid because of left input data', () => {
    const username = component.loginForm.controls['username'];
    username.setValue('pepito@konecta.com');
    expect(component.loginForm.invalid).toBeTrue();
  });

  it('login form must be valid', () => {
    const username = component.loginForm.controls['username'];
    const password = component.loginForm.controls['password'];
    username.setValue('pepito@konecta.com');
    password.setValue('12345678');
    expect(component.loginForm.invalid).toBeFalse();
  });

  it('authService login() should called', () => {
    const username = component.loginForm.controls['username'];
    const password = component.loginForm.controls['password'];
    username.setValue('pepito@konecta.com');
    password.setValue('12345678');
    fixture.detectChanges();
    const button = fixture.debugElement.query(By.css('button'));
    button.nativeElement.click();
    fixture.detectChanges();
    expect(authServiceSpy.login).toHaveBeenCalled();
  });
});
