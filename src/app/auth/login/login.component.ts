import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { finalize } from 'rxjs';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { AuthService } from '../services/auth.service';
import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  isLoading : boolean = false;
  loginForm : FormGroup;

  constructor(
    private _formBuilder : FormBuilder,
    private _authService : AuthService,
    private _localStorageService : LocalStorageService,
    private _router : Router,
    private _message : NzMessageService,
    private _idle: Idle
  ) { 
    this.loginForm = this._formBuilder.group({
      username : ['',[Validators.required,Validators.email]],
      password : ['',[Validators.required]]
    })
  }

  ngOnInit(): void {
  }

  onSubmitLogin(){
    if (this.loginForm.valid) {
      this.isLoading = true;
      this._authService.login(this.loginForm.value)
      .pipe(
        finalize( () => this.isLoading = false )
      ).subscribe({
        next: (resp) => {
          this._localStorageService.setInfoToken(JSON.stringify(resp.data));
          this._localStorageService.setInfoUser(JSON.stringify(resp.data.user));
          this._idle.watch();
          if(resp.data.user.roles.find( (role) => role.id == 1 || role.id == 2 )){
            this._router.navigate(['/admin']);
          }else{
            this._router.navigate(['/']);
          }
          
        },
        error: (resp) => {
          if (resp.status == 422) {
            this._message.error('Credenciales invalidos');
          }else{
            this._message.error('Servicio no disponible. Intenta más tarde.');
          }
          
        }
      })
    }
  }

}
