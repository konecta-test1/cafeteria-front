import { UserInterface } from "src/app/admin/interfaces/user/user-interface";

export interface TokenInterface {
    token_type : string;
    expires_in : number;
    access_token : string;
    refresh_token : string;
    user : UserInterface;
}
