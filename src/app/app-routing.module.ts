import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccessPanelGuard } from './shared/guards/access-panel.guard';
import { AuthenticatedGuard } from './shared/guards/authenticated.guard';

const routes: Routes = [
  { path: '' , loadChildren: () => import('./client/client.module').then(m => m.ClientModule) },
  { path: 'auth' , loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule) },
  { 
    path: 'admin', 
    loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule),
    canLoad: [AuthenticatedGuard,AccessPanelGuard]
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
