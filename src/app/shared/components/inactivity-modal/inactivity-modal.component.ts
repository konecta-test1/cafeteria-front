import { Component, OnInit } from '@angular/core';
import { Idle } from '@ng-idle/core';

@Component({
  selector: 'app-inactivity-modal',
  templateUrl: './inactivity-modal.component.html',
  styleUrls: ['./inactivity-modal.component.less']
})
export class InactivityModalComponent implements OnInit {

  countDown;

  constructor(
    private _idle: Idle, 
  ) {
    this.countDown = this._idle.onTimeoutWarning
  }

  ngOnInit(): void {
  }

}
