import { ResultStatus } from "../enums/result-status.enum";

export interface ResultInterface {
    show : boolean;
    status : ResultStatus;
    title : string;
    description : string;
}
