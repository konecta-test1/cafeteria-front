export enum Messages {
    CREATED_RESOURCE = "Creado correctamente",
    UPDATED_RESOURCE = "Actualizado correctamente",
    DELETED_RESOURCE = "Eliminado correctamente",
    CREATE_RESOURCE_FAILED = "Error en guardar",
    UPDATE_RESOURCE_FAILED = "Error en actualizar",
    DELETE_RESOURCE_FAILED = "Error en eliminar",
  }