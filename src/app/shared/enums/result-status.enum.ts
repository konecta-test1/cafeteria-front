export enum ResultStatus {
    SUCCESS = "success",
    WARNING = "warning",
    INFO = "info",
    ERROR = "error",
}