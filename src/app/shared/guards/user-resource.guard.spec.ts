import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { UserResourceGuard } from './user-resource.guard';

describe('UserResourceGuard', () => {
  let guard: UserResourceGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
      ]
    });
    guard = TestBed.inject(UserResourceGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
