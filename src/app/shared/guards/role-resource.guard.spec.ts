import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { RoleResourceGuard } from './role-resource.guard';

describe('RoleResourceGuard', () => {
  let guard: RoleResourceGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
      ]
    });
    guard = TestBed.inject(RoleResourceGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
