import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { AccessPanelGuard } from './access-panel.guard';

describe('AccessPanelGuard', () => {
  let guard: AccessPanelGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
      ]
    });
    guard = TestBed.inject(AccessPanelGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
