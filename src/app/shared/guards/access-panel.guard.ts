import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { PermissionsUserService } from '../services/permissions-user.service';

@Injectable({
  providedIn: 'root'
})
export class AccessPanelGuard implements CanLoad {

  constructor(
    public readonly _permissionUserService : PermissionsUserService,
    private _location: Location
  ){}

  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let granted = this._permissionUserService.checkPermissions(['access.panel']);
    if (granted) {
      return true;
    }else{
      this._location.back();
      return false;
    }
  }
}
