import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { PermissionsUserService } from '../services/permissions-user.service';

@Injectable({
  providedIn: 'root'
})
export class ProductResourceGuard implements CanActivate {

  constructor(
    public readonly _permissionUserService : PermissionsUserService,
    private _location: Location
  ){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let granted = this._permissionUserService.checkPermissions(['products.read','products.*']);
    if (granted) {
      return true;
    }else{
      this._location.back();
      return false;
    }
  }
  
}
