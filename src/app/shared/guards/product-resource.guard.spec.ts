import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { ProductResourceGuard } from './product-resource.guard';

describe('ProductResourceGuard', () => {
  let guard: ProductResourceGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
      ]
    });
    guard = TestBed.inject(ProductResourceGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
