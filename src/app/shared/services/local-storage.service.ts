import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UserInterface } from 'src/app/admin/interfaces/user/user-interface';
import { TokenInterface } from 'src/app/auth/interfaces/token-interface';
import { Variables } from '../enums/variables.enum';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  private localStorage : Storage = localStorage;

  constructor(
    private _router : Router
  ) { }

  setInfoToken(infoToken:string) : void {
    this.localStorage.setItem(Variables.TOKEN_LOCAL_STORAGE,infoToken);
  }

  setInfoUser(infoUser:string) : void{
    this.localStorage.setItem(Variables.USER_LOCAL_STORAGE,infoUser);
  }

  getInfoToken() : TokenInterface | null {
    let infoToken = this.localStorage.getItem(Variables.TOKEN_LOCAL_STORAGE);
    return infoToken ? JSON.parse(infoToken) : null;
  }

  getToken() : string | null {
    let infoToken =  this.getInfoToken();
    if (infoToken) {
      let {access_token} = infoToken
      return access_token;
    }
    return null;
  }

  getRefreshToken() : string | null {
    let infoToken =  this.getInfoToken();
    if (infoToken) {
      let {refresh_token} = infoToken
      return refresh_token;
    }
    return null;
  }

  getUser() : UserInterface | null {
    let infoUser =  this.localStorage.getItem(Variables.USER_LOCAL_STORAGE);
    return infoUser ? JSON.parse(infoUser) : null;
  }

  clearTokenStorageAndRedirectLogin(path:string="/"){
    this.localStorage.clear();
    this._router.navigate([path]);
  }
}
