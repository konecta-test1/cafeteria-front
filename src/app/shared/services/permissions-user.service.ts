import { Injectable } from '@angular/core';
import { PermissionInterface } from 'src/app/admin/interfaces/permission/permission-interface';
import { UserInterface } from 'src/app/admin/interfaces/user/user-interface';
import { LocalStorageService } from './local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class PermissionsUserService {

  private permissions : PermissionInterface[] = [];

  constructor(
    private _localStorageService : LocalStorageService
  ) {
    const user = this._localStorageService.getUser();
    this.permissions = user?.all_permissions ?? [];
  }

  checkPermissions(requestedPermissions:string[] = []){
    const user = this._localStorageService.getUser();
    const permissions = user?.all_permissions ?? [];
    let permissionGiven = 0;

    requestedPermissions.forEach(requestedPermission => {
      let foundPermission = permissions.find( item => item.name == requestedPermission);
      if(foundPermission) permissionGiven ++;
    });

    return permissionGiven > 0 ? true : false;
  }

}
