import { Injectable, Injector } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { catchError, map, Observable, switchMap, take, throwError } from 'rxjs';
import { AuthService } from 'src/app/auth/services/auth.service';
import { LocalStorageService } from './local-storage.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  refreshTokenInProgress = false;
  isRefreshingToken = false;
  constructor(
    private _authService : AuthService,
    private _localStorageService : LocalStorageService,
    private injector: Injector
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let authRequest = this.addTokenHeader(request,this._localStorageService.getToken() ?? '');
    
    return next.handle(authRequest).pipe(
      catchError(errorData => {
        if (errorData.status === 401){
          // refresh token logic
          if (!this.isRefreshingToken) {
            return this.handleRefreshToken(request,next);
          }
        }
        return throwError(errorData)
        
      })
    );
  }

  handleRefreshToken(request: HttpRequest<unknown>, next: HttpHandler){
    this.isRefreshingToken = true;
    return this._authService.refreshToken().pipe(
      switchMap((resp:any)=>{
        this._localStorageService.setInfoToken(JSON.stringify(resp.data));
        return next.handle(this.addTokenHeader(request,resp.data.access_token));
      }),
      catchError( errorData => {
        this._localStorageService.clearTokenStorageAndRedirectLogin();
        return throwError(errorData);
      })
    )
  }

  addTokenHeader(request: HttpRequest<unknown>, token : string){
    return request.clone({
      headers: request.headers.set('Authorization',`Bearer ${token}`)
    })
  }
}
