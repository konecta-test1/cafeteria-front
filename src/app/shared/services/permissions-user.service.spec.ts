import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { PermissionsUserService } from './permissions-user.service';

describe('PermissionsService', () => {
  let service: PermissionsUserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
      ]
    });
    service = TestBed.inject(PermissionsUserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
