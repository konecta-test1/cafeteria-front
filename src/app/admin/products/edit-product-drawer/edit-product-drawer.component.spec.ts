import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzDrawerRef } from 'ng-zorro-antd/drawer';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzMessageModule, NzMessageService } from 'ng-zorro-antd/message';
import { NzNotificationModule, NzNotificationService } from 'ng-zorro-antd/notification';
import { NzSelectModule } from 'ng-zorro-antd/select';

import { EditProductDrawerComponent } from './edit-product-drawer.component';

describe('EditProductDrawerComponent', () => {
  let component: EditProductDrawerComponent;
  let fixture: ComponentFixture<EditProductDrawerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditProductDrawerComponent ],
      imports : [
        HttpClientTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NzMessageModule,
        NzNotificationModule,
        NzFormModule,
        NzInputModule,
        NzInputNumberModule,
        NzSelectModule,
      ],
      providers: [
        NzMessageService,
        NzDrawerRef,
        NzNotificationService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditProductDrawerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
