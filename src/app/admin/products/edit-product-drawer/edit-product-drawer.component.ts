import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzDrawerRef } from 'ng-zorro-antd/drawer';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { finalize } from 'rxjs';
import { Messages } from 'src/app/shared/enums/messages.enum';
import { ProductInterface } from '../../interfaces/product/product-interface';
import { UpdateProductInterface } from '../../interfaces/product/update-product-interface';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-edit-product-drawer',
  templateUrl: './edit-product-drawer.component.html',
  styleUrls: ['./edit-product-drawer.component.less']
})
export class EditProductDrawerComponent implements OnInit {

  @Input() productToEdit : ProductInterface;
  formEditProduct : FormGroup;
  isLoading : boolean = false;

  constructor(
    private _formBuilder : FormBuilder,
    private _productService : ProductService,
    private _message: NzMessageService,
    private _drawer : NzDrawerRef,
    private _notification: NzNotificationService
  ) {
    this.productToEdit = {
      id : 0,
      name : '',
      price : 0,
      stock : 0,
      minimum_stock : 0,
      datetime : ''
    };
    this.formEditProduct = this._formBuilder.group({
      name : ['',[Validators.required,Validators.minLength(3)]],
      stock : [0,[Validators.required,Validators.min(0)]],
      price : [0,[Validators.required]],
      minimum_stock : ['',[Validators.required,Validators.min(0)]]
    });
  } 

  ngOnInit(): void {
    this.formEditProduct.patchValue(this.productToEdit);
  }

  formatterPesos = (value: number): string => `$ ${value}`;

  onSubmitFormEditProduct(dataForm : UpdateProductInterface){
    if (this.formEditProduct.valid) {
      this.isLoading = true;
      this._productService.updateProduct(this.productToEdit.id , dataForm)
      .pipe(
        finalize(()=>this.isLoading = false)
      ).subscribe({
        next : (resp) => {
          this._message.create('success',Messages.UPDATED_RESOURCE);
          this._drawer.close(true);
        },
        error: (resp) => {
          if (resp.status == 422) {
            let { error : {errors} } = resp.error;
            let propertiesFailed : string[] = Object.keys(errors);
            let errorMsgs : string[] = Object.values(errors);

            propertiesFailed.map( key => {  
              this.formEditProduct.controls[key].setErrors({incorrect:true});
              this._notification.error(errors[key][0],'',{nzPlacement : 'bottomRight'});
            });
          }
        }
      })
    }
  }

  closeDrawer(){
    this._drawer.close(false);
  }
}
