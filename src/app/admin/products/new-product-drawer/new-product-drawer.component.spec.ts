import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NzDrawerModule, NzDrawerRef } from 'ng-zorro-antd/drawer';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzMessageModule, NzMessageService } from 'ng-zorro-antd/message';
import { NzNotificationModule, NzNotificationService } from 'ng-zorro-antd/notification';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NewProductDrawerComponent } from './new-product-drawer.component';


describe('ProductDrawerComponent', () => {
  let component: NewProductDrawerComponent;
  let fixture: ComponentFixture<NewProductDrawerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewProductDrawerComponent ],
      imports: [
        ReactiveFormsModule,
        HttpClientTestingModule,
        NzMessageModule,
        NzDrawerModule,
        NzNotificationModule,
        NzFormModule,
        NzInputModule,
        NzInputNumberModule,
        NzSelectModule,
      ],
      providers: [
        NzMessageService,
        NzDrawerRef,
        NzNotificationService,
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewProductDrawerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
