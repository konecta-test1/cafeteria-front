import { Component, OnInit, Renderer2, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzDrawerRef } from 'ng-zorro-antd/drawer';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { finalize, map } from 'rxjs';
import { Messages } from 'src/app/shared/enums/messages.enum';
import { CreateProductInterface } from '../../interfaces/product/create-product-interface';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-new-product-drawer',
  templateUrl: './new-product-drawer.component.html',
  styleUrls: ['./new-product-drawer.component.less']
})
export class NewProductDrawerComponent implements OnInit {

  formNewProduct : FormGroup;
  isLoading : boolean = false;

  constructor(
    private _formBuilder : FormBuilder,
    private _productService : ProductService,
    private _message: NzMessageService,
    private _drawer : NzDrawerRef,
    private _notification: NzNotificationService
  ) { 
    this.formNewProduct = this._formBuilder.group({
      name : ['',[Validators.required,Validators.minLength(3)]],
      stock : [0,[Validators.required,Validators.min(0)]],
      price : [0,[Validators.required,Validators.min(0)]],
      minimum_stock : ['',[Validators.required,Validators.min(0)]]
    });
  }

  ngOnInit(): void {
  }

  formatterPesos = (value: number): string => `$ ${value}`;

  closeDrawer(){
    this._drawer.close(false);
  }

  onSubmitFormNewProduct(dataForm : CreateProductInterface){
    if (this.formNewProduct.valid) {
      this.isLoading = true;
      this._productService.storeProduct(dataForm)
      .pipe(
        finalize( () => this.isLoading=false )
      ).subscribe({
        next : (resp) => {
          this._message.create('success',Messages.CREATED_RESOURCE);
          this._drawer.close(true);
        },
        error: (resp) => {
          if (resp.status == 422) {
            let { error : {errors} } = resp.error;
            let propertiesFailed : string[] = Object.keys(errors);

            propertiesFailed.map( key => {
              this.formNewProduct.controls[key].setErrors({incorrect:true});
              this._notification.error(errors[key][0],'',{nzPlacement : 'bottomRight'});
            });
          }
        }
      })
    }
  }
}
