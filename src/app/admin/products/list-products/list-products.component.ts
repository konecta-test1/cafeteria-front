import { Component, OnDestroy, OnInit } from '@angular/core';
import { NzDrawerService } from 'ng-zorro-antd/drawer';
import { NzMessageService } from 'ng-zorro-antd/message';
import { finalize, map, Observable, Subject, take } from 'rxjs';
import { Messages } from 'src/app/shared/enums/messages.enum';
import { PermissionsUserService } from 'src/app/shared/services/permissions-user.service';
import { ProductInterface } from '../../interfaces/product/product-interface';
import { ProductService } from '../../services/product.service';
import { EditProductDrawerComponent } from '../edit-product-drawer/edit-product-drawer.component';
import { NewProductDrawerComponent } from '../new-product-drawer/new-product-drawer.component';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.less']
})
export class ListProductsComponent implements OnInit {

  products$! : Observable<ProductInterface[]>;
  isLoading : boolean = false;
  permissionsRequired = {
    create : false,
    update : false,
    delete : false
  };

  constructor(
    private _drawerService: NzDrawerService,
    private _message: NzMessageService,
    private readonly _productService : ProductService,
    public readonly _permissionUserService : PermissionsUserService
  ) { 
    
  }

  ngOnInit(): void {
    this.checkPermissionsOfUser();
    this.getAllProducts();
  }

  checkPermissionsOfUser(){
    this.permissionsRequired.create = this._permissionUserService.checkPermissions(['products.create','products.*']);
    this.permissionsRequired.update = this._permissionUserService.checkPermissions(['products.update','products.*']);
    this.permissionsRequired.delete = this._permissionUserService.checkPermissions(['products.delete','products.*']);
  }

  showAddNewProductDrawer(){
    const drawerRef = this._drawerService.create<NewProductDrawerComponent, { value: string }, string>({
      nzClosable : false,
      nzWidth : 500,
      nzTitle: 'Nuevo Producto',
      nzFooter: '',
      nzExtra: '',
      nzContent: NewProductDrawerComponent
    });

    drawerRef.afterClose.pipe( take(1) ).subscribe((created) => {
      if(created) this.getAllProducts();
    });
  }

  showEditProductDrawer(product:ProductInterface){
    const drawerRef = this._drawerService.create<EditProductDrawerComponent, { productToEdit: ProductInterface }, boolean>({
      nzClosable : false,
      nzWidth : 500,
      nzTitle: `Editar Producto - ${product.name}`,
      nzFooter: '',
      nzExtra: '',
      nzContentParams : { productToEdit : product },
      nzContent: EditProductDrawerComponent
    });

    drawerRef.afterClose.pipe( take(1) ).subscribe((created) => {
      if(created) this.getAllProducts();
    });
  }

  getAllProducts(){
    this.isLoading = true;
    this.products$ = this._productService.getAllProducts().pipe(
      map(resp => resp.data),
      finalize( () => this.isLoading = false )
    );
  }

  deleteProduct(productId:number){
    this.isLoading = true;
    this._productService.deleteProduct(productId)
    .pipe(
      finalize( () => this.isLoading = false )
    ).subscribe({
      next: (resp) => {
        this.getAllProducts();
        this._message.create('success',Messages.DELETED_RESOURCE);
      },
      error: (error) => {
        this._message.create('error',Messages.DELETE_RESOURCE_FAILED);
      }
    });
  }

}
