import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { IconsProviderModule } from '../icons-provider.module';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { ListProductsComponent } from './products/list-products/list-products.component';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NewProductDrawerComponent } from './products/new-product-drawer/new-product-drawer.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { ListUsersComponent } from './users/list-users/list-users.component';
import { HeadPageComponent } from './head-page/head-page.component';
import { UserDrawerComponent } from './users/user-drawer/user-drawer.component';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { ListRolesComponent } from './roles/list-roles/list-roles.component';
import { RoleDrawerComponent } from './roles/role-drawer/role-drawer.component';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditProductDrawerComponent } from './products/edit-product-drawer/edit-product-drawer.component';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzNotificationModule } from 'ng-zorro-antd/notification';

@NgModule({
  declarations: [
    DashboardComponent,
    ListProductsComponent,
    NewProductDrawerComponent,
    ListUsersComponent,
    HeadPageComponent,
    UserDrawerComponent,
    ListRolesComponent,
    RoleDrawerComponent,
    EditProductDrawerComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AdminRoutingModule,
    IconsProviderModule,
    NzLayoutModule,
    NzMenuModule,
    NzTableModule,
    NzGridModule,
    NzButtonModule,
    NzDrawerModule,
    NzFormModule,
    NzInputModule,
    NzInputNumberModule,
    NzSelectModule,
    NzDividerModule,
    NzPopconfirmModule,
    NzMessageModule,
    NzCardModule,
    NzAvatarModule,
    NzToolTipModule,
    NzSpinModule,
    NzNotificationModule,
  ]
})
export class AdminModule { }
