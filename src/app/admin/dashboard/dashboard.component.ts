import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs';
import { AuthService } from 'src/app/auth/services/auth.service';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';
import { PermissionsUserService } from 'src/app/shared/services/permissions-user.service';
import { UserInterface } from '../interfaces/user/user-interface';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent implements OnInit {
  
  userName : string = '';
  isCollapsed : boolean= false;
  isLoading : boolean = false;
  permissionsRequired = {
    products : false,
    users : false,
    roles : false
  };

  constructor(
    private _authService : AuthService,
    private _localStorageService : LocalStorageService,
    public readonly _permissionUserService : PermissionsUserService
  ) { 
    this.userName = this._localStorageService.getUser()?.name ?? '';
  }

  ngOnInit(): void {
    this.checkPermissionsOfUser();

  }

  toLogout(){
    this.isLoading = true;
    this._authService.logout().pipe(
      finalize( () => {
        this.isLoading = false;
        this._localStorageService.clearTokenStorageAndRedirectLogin();
      })
    ).subscribe();
  }

  checkPermissionsOfUser(){
    this.permissionsRequired.products = this._permissionUserService.checkPermissions(['products.read','products.*']);
    this.permissionsRequired.users = this._permissionUserService.checkPermissions(['users.read','users.*']);
    this.permissionsRequired.roles = this._permissionUserService.checkPermissions(['roles.read','roles.*']);
  }
}
