import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-head-page',
  templateUrl: './head-page.component.html',
  styleUrls: ['./head-page.component.less']
})
export class HeadPageComponent implements OnInit {

  @Input('title') title : string = ''; 
  @Input('permissionToCreate') permissionToCreate : boolean = false; 
  @Output() addButtonClicked = new EventEmitter();
  
  constructor() { }

  ngOnInit(): void {
  }

  clickAddButton(){
    this.addButtonClicked.emit();
  }
}
