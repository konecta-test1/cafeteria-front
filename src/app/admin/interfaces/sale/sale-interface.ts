import { ProductInterface } from "../product/product-interface";
import { UserInterface } from "../user/user-interface";

export interface SaleInterface {
    id: number;
    user: UserInterface;
    datetime: string;
    products: ProductInterface[];
}
