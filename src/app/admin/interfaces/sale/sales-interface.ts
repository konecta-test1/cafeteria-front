import { UserInterface } from "../user/user-interface";

export interface SalesInterface {
    id: number;
    user: UserInterface;
    datetime: string;
}
