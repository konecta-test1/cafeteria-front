import { ProductSaleInterface } from "./product-sale-interface";

export interface CreateSaleInterface {
    user_id : number;
    products : ProductSaleInterface[]
}
