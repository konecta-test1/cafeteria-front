export interface CreateRoleInterface {
    name : string;
    permissions : Array<number>
}
