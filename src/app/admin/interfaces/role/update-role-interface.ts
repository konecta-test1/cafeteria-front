export interface UpdateRoleInterface {
    name? : string;
    permissions? : Array<number>
}
