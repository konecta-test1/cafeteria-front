export interface StandarApiResponseInterface <AnotherInterface> {
    status : boolean;
    data : AnotherInterface;
}
