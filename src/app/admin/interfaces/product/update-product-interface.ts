export interface UpdateProductInterface {
    name? : string;
    stock? : number;
    price? : number;
    minimum_stock? : number;
}
