export interface ProductInterface {
    id : number;
    name : string;
    stock : number;
    price : number;
    minimum_stock : number;
    datetime : string;
}
