export interface CreateProductInterface {
    name : string;
    stock : number;
    price : number;
    minimum_stock : number;
}
