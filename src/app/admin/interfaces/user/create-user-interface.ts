export interface CreateUserInterface {
    id: number;
    name: string;
    email: string;
    roles: Array<number>;
}
