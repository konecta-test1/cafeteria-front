import { PermissionInterface } from "../permission/permission-interface";
import { RoleInterface } from "../role/role-interface";

export interface UserInterface {
    id: number;
    name: string;
    email: string;
    roles: RoleInterface[];
    all_permissions? : PermissionInterface[]
}
