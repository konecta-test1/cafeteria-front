export interface UpdateUserInterface {
    id?: number;
    name?: string;
    email?: string;
    roles?: Array<number>;
}
