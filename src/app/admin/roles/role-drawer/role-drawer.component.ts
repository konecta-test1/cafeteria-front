import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzDrawerRef } from 'ng-zorro-antd/drawer';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { TransferItem } from 'ng-zorro-antd/transfer';
import { finalize, map, Observable } from 'rxjs';
import { Messages } from 'src/app/shared/enums/messages.enum';
import { PermissionInterface } from '../../interfaces/permission/permission-interface';
import { RoleInterface } from '../../interfaces/role/role-interface';
import { PermissionService } from '../../services/permission.service';
import { RoleService } from '../../services/role.service';

@Component({
  selector: 'app-role-drawer',
  templateUrl: './role-drawer.component.html',
  styleUrls: ['./role-drawer.component.less']
})
export class RoleDrawerComponent implements OnInit {

  @Input() roleId: number = 0;
  isLoading : boolean = false;
  permissions$ : Observable<PermissionInterface[]>;
  permissionSelected : any;
  formRole : FormGroup;

  constructor(
    private _formBuilder : FormBuilder,
    public _drawer : NzDrawerRef,
    private _roleService : RoleService,
    private _permissionService : PermissionService,
    private _message : NzMessageService,
    private _notification : NzNotificationService
  ) {
    this.permissions$ = this._permissionService.getAllPermissions().pipe( map( (resp) => resp.data ) );
    this.formRole = this._formBuilder.group({
      name : ['',[Validators.required, Validators.minLength(1)]],
      permissions : [[],Validators.required]
    });
  }

  ngOnInit(): void {
    if(this.roleId) this.getRole(this.roleId);
    
  }
  
  onSubmitFormRole(){
    if(this.roleId) this.updateRole();
    else this.createNewRole();  
  }

  createNewRole(){
    if (this.formRole.valid) {
      this.isLoading = true;
      this._roleService.storeRole(this.formRole.value)
      .pipe(
        finalize( () => this.isLoading = false )
      ).subscribe({
        next: (resp) => {
          this._message.create('success',Messages.CREATED_RESOURCE);
          this._drawer.close(true);
        },
        error: (resp) => {
          if (resp.status == 422) {
            let { error : {errors} } = resp.error;
            let propertiesFailed : string[] = Object.keys(errors);

            propertiesFailed.map( key => {
              this.formRole.controls[key].setErrors({incorrect:true});
              this._notification.error(errors[key][0],'',{nzPlacement : 'bottomRight'});
            });
          }
        }
      })
      
    }
    
  }

  updateRole(){
    if (this.formRole.valid) {
      this.isLoading = true;
      this._roleService.updateRole(this.roleId,this.formRole.value)
      .pipe(
        finalize( () => this.isLoading = false )
      ).subscribe({
        next: (resp) => {
          this._message.create('success',Messages.UPDATED_RESOURCE);
          this._drawer.close(true);
        },
        error: (resp) => {
          if (resp.status == 422) {
            let { error : {errors} } = resp.error;
            let propertiesFailed : string[] = Object.keys(errors);

            propertiesFailed.map( key => {
              this.formRole.controls[key].setErrors({incorrect:true});
              this._notification.error(errors[key][0],'',{nzPlacement : 'bottomRight'});
            });
          }
        }
      })
      
    }
    
  }

  getRole(roleId:number){
    this.isLoading = true;
    this._roleService.getOneRole(roleId)
    .pipe(
      finalize( () => this.isLoading = false )
    ).subscribe({
      next: (resp) => {
        this.setRoleInForm(resp.data);
      }
    })
  }

  setRoleInForm(role:RoleInterface){
    this.formRole.setValue({
      name : role.name,
      permissions: role.permissions.map( item => item.id )
    });
  }
}
