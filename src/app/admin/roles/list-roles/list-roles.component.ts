import { Component, OnInit } from '@angular/core';
import { NzDrawerService } from 'ng-zorro-antd/drawer';
import { NzMessageService } from 'ng-zorro-antd/message';
import { finalize, map, Observable } from 'rxjs';
import { Messages } from 'src/app/shared/enums/messages.enum';
import { PermissionsUserService } from 'src/app/shared/services/permissions-user.service';
import { RoleInterface } from '../../interfaces/role/role-interface';
import { RoleService } from '../../services/role.service';
import { RoleDrawerComponent } from '../role-drawer/role-drawer.component';

@Component({
  selector: 'app-list-roles',
  templateUrl: './list-roles.component.html',
  styleUrls: ['./list-roles.component.less']
})
export class ListRolesComponent implements OnInit {

  isLoading : boolean = false;
  roles$! : Observable<RoleInterface[]>;
  permissionsRequired = {
    create : false,
    update : false,
    delete : false
  };
  
  constructor(
    private _drawerService: NzDrawerService,
    private _message : NzMessageService,
    private _roleService : RoleService,
    public readonly _permissionUserService : PermissionsUserService
  ) { }

  ngOnInit(): void {
    this.checkPermissionsOfUser();
    this.getAllRoles();
  }

  checkPermissionsOfUser(){
    this.permissionsRequired.create = this._permissionUserService.checkPermissions(['roles.create','roles.*']);
    this.permissionsRequired.update = this._permissionUserService.checkPermissions(['roles.update','roles.*']);
    this.permissionsRequired.delete = this._permissionUserService.checkPermissions(['roles.delete','roles.*']);
  }

  showAddNewRoleDrawer(){
    // TODO: Refactorizar
    const drawerRef = this._drawerService.create<RoleDrawerComponent, { value: string }, string>({
      nzClosable : false,
      nzWidth : 400,
      nzTitle: 'Nuevo Rol',
      nzFooter: '',
      nzExtra: '',
      nzContent: RoleDrawerComponent
    });

    drawerRef.afterClose.subscribe({
      next: (resp) => resp ? this.getAllRoles() : null
    })
  }

  showEditRoleDrawer(roleToEdit:RoleInterface){
    // TODO: Refactorizar
    const drawerRef = this._drawerService.create<RoleDrawerComponent, { roleId: number }, string>({
      nzClosable : false,
      nzWidth : 400,
      nzTitle: `Editar Rol - ${roleToEdit.name}`,
      nzFooter: '',
      nzExtra: '',
      nzContentParams: {roleId : roleToEdit.id },
      nzContent: RoleDrawerComponent
    });

    drawerRef.afterClose.subscribe({
      next: (resp) => resp ? this.getAllRoles() : null
    })
  }

  getAllRoles(){
    this.isLoading = true;
    this.roles$ = this._roleService.getAllRoles().pipe(
      map(resp => resp.data),
      finalize( () => this.isLoading = false )
    );
  }

  deleteRole(roleId:number){
    this.isLoading = true;
    this._roleService.deleteRole(roleId)
    .pipe(
      finalize( () => this.isLoading = false )
    ).subscribe({
      next: (resp) => {
        this.getAllRoles();
        this._message.create('success',Messages.DELETED_RESOURCE);
      },
      error: (resp) => {
        if (resp.status == 422) {
          this._message.create('error', resp.error.error.message );
        }
      }
    });
  }
}
