import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CreateProductInterface } from '../interfaces/product/create-product-interface';
import { ProductInterface } from '../interfaces/product/product-interface';
import { UpdateProductInterface } from '../interfaces/product/update-product-interface';
import { StandarApiResponseInterface } from '../interfaces/standar-api-response-interface';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(
    private _http : HttpClient
  ) { }

  getAllProducts() : Observable<StandarApiResponseInterface<ProductInterface[]>> {
    return this._http.get<StandarApiResponseInterface<ProductInterface[]>>(`${environment.apiUrl}/products`);
  }

  getOneProduct(produtcId:number) : Observable<StandarApiResponseInterface<ProductInterface>> {
    return this._http.get<StandarApiResponseInterface<ProductInterface>>(`${environment.apiUrl}/products/${produtcId}`);
  }

  storeProduct(productData:CreateProductInterface) : Observable<StandarApiResponseInterface<ProductInterface>> {
    return this._http.post<StandarApiResponseInterface<ProductInterface>>(`${environment.apiUrl}/products`,productData);
  }

  updateProduct(produtcId:number,productData:UpdateProductInterface) : Observable<StandarApiResponseInterface<ProductInterface>> {
    return this._http.put<StandarApiResponseInterface<ProductInterface>>(`${environment.apiUrl}/products/${produtcId}`,productData);
  }

  deleteProduct(produtcId:number) : Observable<StandarApiResponseInterface<ProductInterface>> {
    return this._http.delete<StandarApiResponseInterface<ProductInterface>>(`${environment.apiUrl}/products/${produtcId}`);
  }
}
