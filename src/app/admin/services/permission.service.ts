import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PermissionInterface } from '../interfaces/permission/permission-interface';
import { StandarApiResponseInterface } from '../interfaces/standar-api-response-interface';

@Injectable({
  providedIn: 'root'
})
export class PermissionService {

  constructor(
    private _http : HttpClient
  ) { }

  getAllPermissions() : Observable<StandarApiResponseInterface<PermissionInterface[]>> {
    return this._http.get<StandarApiResponseInterface<PermissionInterface[]>>(`${environment.apiUrl}/permissions`);
  }

}
