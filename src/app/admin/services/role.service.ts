import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CreateRoleInterface } from '../interfaces/role/create-role-interface';
import { RoleInterface } from '../interfaces/role/role-interface';
import { UpdateRoleInterface } from '../interfaces/role/update-role-interface';
import { StandarApiResponseInterface } from '../interfaces/standar-api-response-interface';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  constructor(
    private _http : HttpClient
  ) { }

  getAllRoles() : Observable<StandarApiResponseInterface<RoleInterface[]>> {
    return this._http.get<StandarApiResponseInterface<RoleInterface[]>>(`${environment.apiUrl}/roles`);
  }

  getOneRole(roleId:number) : Observable<StandarApiResponseInterface<RoleInterface>> {
    return this._http.get<StandarApiResponseInterface<RoleInterface>>(`${environment.apiUrl}/roles/${roleId}`);
  }

  storeRole(productData:CreateRoleInterface) : Observable<StandarApiResponseInterface<RoleInterface>> {
    return this._http.post<StandarApiResponseInterface<RoleInterface>>(`${environment.apiUrl}/roles`,productData);
  }

  updateRole(roleId:number,productData:UpdateRoleInterface) : Observable<StandarApiResponseInterface<RoleInterface>> {
    return this._http.put<StandarApiResponseInterface<RoleInterface>>(`${environment.apiUrl}/roles/${roleId}`,productData);
  }

  deleteRole(roleId:number) : Observable<StandarApiResponseInterface<RoleInterface>> {
    return this._http.delete<StandarApiResponseInterface<RoleInterface>>(`${environment.apiUrl}/roles/${roleId}`);
  }
}
