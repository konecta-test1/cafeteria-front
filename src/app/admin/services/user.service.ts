import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { StandarApiResponseInterface } from '../interfaces/standar-api-response-interface';
import { CreateUserInterface } from '../interfaces/user/create-user-interface';
import { UpdateUserInterface } from '../interfaces/user/update-user-interface';
import { UserInterface } from '../interfaces/user/user-interface';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private _http : HttpClient
  ) { }

  getAllUsers() : Observable<StandarApiResponseInterface<UserInterface[]>> {
    return this._http.get<StandarApiResponseInterface<UserInterface[]>>(`${environment.apiUrl}/users`);
  }

  getOneUser(userId:number) : Observable<StandarApiResponseInterface<UserInterface>> {
    return this._http.get<StandarApiResponseInterface<UserInterface>>(`${environment.apiUrl}/users/${userId}`);
  }

  storeUser(userData:CreateUserInterface) : Observable<StandarApiResponseInterface<UserInterface>> {
    return this._http.post<StandarApiResponseInterface<UserInterface>>(`${environment.apiUrl}/users`,userData);
  }

  updateUser(userId:number,userData:UpdateUserInterface) : Observable<StandarApiResponseInterface<UserInterface>> {
    return this._http.put<StandarApiResponseInterface<UserInterface>>(`${environment.apiUrl}/users/${userId}`,userData);
  }

  deleteUser(userId:number) : Observable<StandarApiResponseInterface<UserInterface>> {
    return this._http.delete<StandarApiResponseInterface<UserInterface>>(`${environment.apiUrl}/users/${userId}`);
  }
}
