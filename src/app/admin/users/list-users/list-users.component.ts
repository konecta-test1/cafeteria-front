import { Component, OnInit } from '@angular/core';
import { NzDrawerService } from 'ng-zorro-antd/drawer';
import { NzMessageService } from 'ng-zorro-antd/message';
import { finalize, map, Observable } from 'rxjs';
import { Messages } from 'src/app/shared/enums/messages.enum';
import { PermissionsUserService } from 'src/app/shared/services/permissions-user.service';
import { UserInterface } from '../../interfaces/user/user-interface';
import { UserService } from '../../services/user.service';
import { UserDrawerComponent } from '../user-drawer/user-drawer.component';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.less']
})
export class ListUsersComponent implements OnInit {

  isLoading : boolean = false;
  users$ : Observable<UserInterface[]>;
  permissionsRequired = {
    create : false
  };
  
  constructor(
    private _drawerService: NzDrawerService,
    private _message: NzMessageService,
    private _userService : UserService,
    public readonly _permissionUserService : PermissionsUserService
  ) {
    this.users$ = this._userService.getAllUsers().pipe( map( resp => resp.data ));
   }

  ngOnInit(): void {
    this.checkPermissionsOfUser();
  }

  checkPermissionsOfUser(){
    this.permissionsRequired.create = this._permissionUserService.checkPermissions(['users.create','users.*']);
  }

  showAddNewUserDrawer(){
    // TODO: Refactorizar
    const drawerRef = this._drawerService.create<UserDrawerComponent, { value: string }, string>({
      nzClosable : false,
      nzHeight : 170,
      nzPlacement: 'top',
      nzTitle: 'Nuevo Usuario',
      nzFooter: '',
      nzExtra: '',
      nzContent: UserDrawerComponent
    });

    drawerRef.afterClose.subscribe({
      next: (resp) => resp ? this.getAllUsers() : null
    })
  }

  showEditUserDrawer(user:UserInterface){
    // TODO: Refactorizar
    const drawerRef = this._drawerService.create<UserDrawerComponent, { userId: number }, string>({
      nzClosable : false,
      nzHeight : 170,
      nzPlacement: 'top',
      nzTitle: `Editar Usuario - ${user.name}`,
      nzFooter: '',
      nzExtra: '',
      nzContentParams: { userId : user.id },
      nzContent: UserDrawerComponent
    });

    drawerRef.afterClose.subscribe({
      next: (resp) => resp ? this.getAllUsers() : null
    })
  }

  getAllUsers(){
    this.users$ = this._userService.getAllUsers().pipe( map( resp => resp.data ));
  }

  deleteUser(userId:number){
    this.isLoading = true;
    this._userService.deleteUser(userId)
    .pipe(
      finalize( () => this.isLoading = false )
    ).subscribe({
      next: (resp) => {
        this.getAllUsers();
        this._message.create('success',Messages.DELETED_RESOURCE);
      },
      error: (resp) => {
        if (resp.status == 422) {
          this._message.create('error', resp.error.error.message );
        }
      }
    });
  }
}
