import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzDrawerRef } from 'ng-zorro-antd/drawer';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { finalize, map, Observable } from 'rxjs';
import { Messages } from 'src/app/shared/enums/messages.enum';
import { RoleInterface } from '../../interfaces/role/role-interface';
import { UserInterface } from '../../interfaces/user/user-interface';
import { RoleService } from '../../services/role.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-user-drawer',
  templateUrl: './user-drawer.component.html',
  styleUrls: ['./user-drawer.component.less']
})
export class UserDrawerComponent implements OnInit {

  @Input() userId: number = 0;
  isLoading : boolean = false;
  roles$ : Observable<RoleInterface[]>;
  formUser : FormGroup;

  constructor(
    private _formBuilder : FormBuilder,
    public _drawer : NzDrawerRef,
    private _userService : UserService,
    private _roleService : RoleService,
    private _message : NzMessageService,
    private _notification : NzNotificationService
  ) { 
    this.roles$ = this._roleService.getAllRoles().pipe( map( (resp) => resp.data ) );
    this.formUser = this._formBuilder.group({
      name : ['',[Validators.required, Validators.minLength(1)]],
      email : ['',[Validators.required, Validators.email]],
      roles : [[],Validators.required]
    });
  }

  ngOnInit(): void {
    if(this.userId) this.getUser(this.userId);
    
  }

  getUser(userId:number){
    this.isLoading = true;
    this._userService.getOneUser(userId)
    .pipe(
      finalize( () => this.isLoading = false )
    ).subscribe({
      next: (resp) => {
        this.setUserInForm(resp.data);
      }
    })
  }

  setUserInForm(user:UserInterface){
    this.formUser.setValue({
      name : user.name,
      email : user.email,
      roles: user.roles.map( item => item.id )
    });
  }

  onSubmitFormUser(){
    if(this.userId) this.updateUser();
    else this.createNewUser();  
  }

  createNewUser(){
    if (this.formUser.valid) {
      this.isLoading = true;
      this._userService.storeUser(this.formUser.value)
      .pipe(
        finalize( () => this.isLoading = false )
      ).subscribe({
        next: (resp) => {
          this._message.create('success',Messages.CREATED_RESOURCE);
          this._drawer.close(true);
        },
        error: (resp) => {
          if (resp.status == 422) {
            let { error : {errors} } = resp.error;
            let propertiesFailed : string[] = Object.keys(errors);

            propertiesFailed.map( key => {
              this.formUser.controls[key].setErrors({incorrect:true});
              this._notification.error(errors[key][0],'',{nzPlacement : 'bottomRight'});
            });
          }
        }
      })
      
    }
    
  }

  updateUser(){
    if (this.formUser.valid) {
      this.isLoading = true;
      this._userService.updateUser(this.userId,this.formUser.value)
      .pipe(
        finalize( () => this.isLoading = false )
      ).subscribe({
        next: (resp) => {
          this._message.create('success',Messages.CREATED_RESOURCE);
          this._drawer.close(true);
        },
        error: (resp) => {
          if (resp.status == 422) {
            let { error : {errors} } = resp.error;
            let propertiesFailed : string[] = Object.keys(errors);

            propertiesFailed.map( key => {
              this.formUser.controls[key].setErrors({incorrect:true});
              this._notification.error(errors[key][0],'',{nzPlacement : 'bottomRight'});
            });
          }
        }
      })
      
    }
    
  }
}
