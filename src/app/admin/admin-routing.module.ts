import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductResourceGuard } from '../shared/guards/product-resource.guard';
import { RoleResourceGuard } from '../shared/guards/role-resource.guard';
import { UserResourceGuard } from '../shared/guards/user-resource.guard';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ListProductsComponent } from './products/list-products/list-products.component';
import { ListRolesComponent } from './roles/list-roles/list-roles.component';
import { ListUsersComponent } from './users/list-users/list-users.component';

const routes: Routes = [
  { path: '', component: DashboardComponent, children: [
    { path: 'products', component: ListProductsComponent, canActivate: [ProductResourceGuard] },
    { path: 'users', component: ListUsersComponent, canActivate: [UserResourceGuard] },
    { path: 'roles', component: ListRolesComponent, canActivate: [RoleResourceGuard] },
    { path: '**', redirectTo: '' }
  ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
