# Cafeteria Front

## Requirements

- Node v16.15.1
- Angular v13.2.0

## Installation

Clone repository

```
git clone https://gitlab.com/konecta-test1/cafeteria-front.git
```

Install dependencies:
```
npm install
```
